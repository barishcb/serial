Some basic tools that help your day to day life as an animation/VFX artist

:Author: `Barish Chandran B <http://www.barishcb.com>`_

.. contents::
    :local:
    :depth: 2
    :backlinks: none

.. sectnum::

Introduction
============

What is bcSerial?
----------------

``bcSerial`` conatins some basic tools for animation/VFX artists.

How is bcSerial licensed?
------------------------

`MIT License <https://opensource.org/licenses/MIT>`_

Contact details
---------------

Drop me an email to barishcb@gmail.com for any questions regarding ``bcSerial``. For reporting problems with ``bcSerial`` or submitting feature requests, the best way is to open an issue on the `bcSerial project page <https://gitlab.com/barishcb/serial>`_.


Package contents
================

Once you unzip the ``bcSerial`` package, you'll see the following files and directories:

README.rst:
  This README file.

setup.py:
  Installation script

bcSerial/:
  The ``bcSerial`` module source code.

tests/:
  Unit tests.

Contributors
============

`Barish Chandran B <http://www.barishcb.com>`_
