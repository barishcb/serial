"""
"""
import cPickle as _cPickle

from bcSerial import exceptions as _exceptions
from bcSerial.serialization._serializers import abstractSerializer as _absSerializer


class CPickleSerializer(_absSerializer.AbstractSerializer):
	""" docstring for AbstractSerializer
	"""
	serializerName = "cPickle"
	fileExtension = "pkl"

	def encode(self, data):
		""" Method to deserialize the given encoded data. This is a virtal function

			Args:
				data (object): Objects needs to be serialized

			Return:
				(str): Serialized data.

			Raises:
				_exception.EncoderError: When the data is unable to encode
		"""
		serializedData = ""
		try:
			serializedData = _cPickle.dumps(data)
		except _cPickle.PicklingError as err:
			raise _exceptions.SerializationError(err.message)
		return serializedData

	def decode(self, encodedObject):
		""" Method to deserialize the given encoded data. This is a virtal function

			Args:
				encodedObject (str): Serialized data, which needs to decoded

			Return:
				(object): De serailized data

			Raises:
				_exception.DecoderError: When the data is unable to decode
		"""
		data = None
		try:
			data = _cPickle.loads(encodedObject)
		except _cPickle.UnpicklingError as err:
			raise _exceptions.SerializationError(err.message)
		return data
