"""
"""
import json as _json

from bcSerial import exceptions as _exceptions
from bcSerial.serialization._serializers import abstractSerializer as _absSerializer
from bcSerial.utilities import jsonUtil as _jsonUtil


class JSONSerializer(_absSerializer.AbstractSerializer):
	""" docstring for JSONSerializer
	"""
	serializerName = "json"
	fileExtension = "pkl"

	def __init__(self, storage, encoder=None, decoder=None):
		super(JSONSerializer, self).__init__(storage)
		self._encoder = encoder or _json.JSONEncoder
		self._decoder = decoder or _json.JSONDecoder

	def encode(self, data):
		""" Method to deserialize the given encoded data. This is a virtal function

			Args:
				data (object): Objects needs to be serialized

			Return:
				(str): Serialized data.

			Raises:
				_exception.EncoderError: When the data is unable to encode
		"""
		serializedData = ""
		try:
			serializedData = _json.dumps(data, cls=self._encoder)
		except TypeError as err:
			raise _exceptions.SerializationError(err.message)

		return serializedData

	def decode(self, encodedObject):
		""" Method to deserialize the given encoded data. This is a virtal function

			Args:
				encodedObject (str): Serialized data, which needs to decoded

			Return:
				(object): De serailized data

			Raises:
				_exception.DecoderError: When the data is unable to decode
		"""
		data = None
		try:
			data = _json.loads(encodedObject, cls=self._decoder)
		except TypeError as err:
			raise _exceptions.SerializationError(err.message)
		return data
