"""
"""
import os as _os
import abc

from bcSerial import exceptions as _exceptions
from bcSerial.serialization._storages import abstractStorage as _absStorage
from bcSerial.serialization import storages as _storages


class AbstractSerializer(object):
	""" docstring for AbstractSerializer
	"""
	serializerName = ""
	fileExtension = ""

	def __init__(self, storage):
		super(AbstractSerializer, self).__init__()
		self._storage = storage or _storages.getStorageClass("file")(
			_os.path.join(_os.getcwd(), "serializedData.slz")
		)

	@property
	def storage(self):
		""" Getter method for the property storage

			Returns:
				(AbstractStorage): Storage object for the class
		"""
		return self._storage

	@storage.setter
	def storage(self, storage):
		""" Setter method for the property storage

			Args:
				(AbstractStorage): Storage object for the class
		"""
		if not isinstance(storage, _absStorage.AbstractStorage):
			raise _exceptions.SerializationError("Storage Error: Given object is not a storage (%r)" % storage)
		# TODO : Put some validation if need
		self._storage = storage

	def read(self, storage=None):
		"""
		"""
		serailizedData = ""
		storage = storage or self.storage
		try:
			serailizedData = storage.read()
		except _exceptions.StorageError as err:
			raise _exceptions.SerializationError("Storage Error: %s" % err.message)
		return self.decode(serailizedData)

	def write(self, data, storage=None):
		"""
		"""
		serailizedData = self.encode(data)
		storage = storage or self.storage
		result = False
		try:
			result = storage.write(serailizedData)
		except _exceptions.StorageError as err:
			raise _exceptions.SerializationError("Storage Error: %s" % err.message)
		return result

	@abc.abstractmethod
	def encode(self, data):
		""" Method to deserialize the given encoded data. This is a virtal function

			Args:
				data (object): Objects needs to be serialized

			Return:
				(str): Serialized data.

			Raises:
				_exception.EncoderError: When the data is unable to encode
		"""
		raise NotImplementedError("Then 'encode' method needs to be implemented.")

	@abc.abstractmethod
	def decode(self, encodedObject):
		""" Method to deserialize the given encoded data. This is a virtal function

			Args:
				encodedObject (str): Serialized data, which needs to decoded

			Return:
				(object): De serailized data

			Raises:
				_exception.DecoderError: When the data is unable to decode
		"""
		raise NotImplementedError("Then 'decode' method needs to be implemented.")
