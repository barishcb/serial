""" Public interface for storages
"""
from bcSerial import exceptions as _exceptions
from bcSerial.serialization import _storages
from bcSerial.serialization._storages import abstractStorage
from bcSerial.serialization._storages import fileStorage
from bcSerial.utilities import moduleUtils as _moduleUtils

DEFAULT_STORAGETYPE = "file"


def supportedTypes():
	"""Returns all the supported storages for bcSerial

		Returns:
			(list): list of supported storage classes
	"""
	supportedClasses = _moduleUtils.filterAllClasses(
		_storages,
		"bcSerial.serialization._storages",
		[abstractStorage.AbstractStorage]
	)
	return [storageClass.storageName for storageClass in supportedClasses ]


def getStorageClass(storageType):
	"""[summary]

	[description]

	Arguments:
		storageType {[type]} -- [description]
	"""
	if storageType not in supportedTypes():
		_exceptions.UtilitiesError("The given storage type is not supported %s" % storageType)
	supportedClasses = _moduleUtils.filterAllClasses(
		_storages,
		"bcSerial.serialization._storages",
		[abstractStorage.AbstractStorage]
	)
	for storageClass in supportedClasses:
		if storageType == storageClass.storageName:
			return storageClass
