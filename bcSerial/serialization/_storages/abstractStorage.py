"""
"""
import abc

from bcSerial import exceptions as _exceptions


class AbstractStorage(object):
	"""
	"""
	storageName = ""
	storageType = ""
	def __init__(self, source):
		super(AbstractStorage, self).__init__()
		self._source = None
		self.source = source

	@property
	def source(self):
		""" Getter method for the property source

			Returns:
				(Abstractsource): source object for the class
		"""
		return self._source

	@source.setter
	def source(self, source):
		""" Setter method for the property source

			Args:
				(Abstractsource): source object for the class
		"""
		if self._validateSource(source):
			self._source = source

	def read(self):
		""" Method to read/get the data from the given storage

			Return:
				(str): The data read from the storage
		"""
		valdation = self._validate()
		if valdation:
			raise _exceptions.StorageError("The give storage is not valid")
		return self._readData()

	def write(self, data):
		""" Method to write/send the data to the given storage

			Return:
				(bool): True if the storage is valid
		"""
		valdation = self._validate()
		if valdation:
			raise _exceptions.StorageError("The give storage is not valid")
		return self._writeData(data)

	@abc.abstractmethod
	def _validate(self):
		""" Method to validate the storage.

			Return:
				(bool): True if the storage is valid
		"""
		raise NotImplementedError("Then 'validate' method needs to be implemented.")

	@abc.abstractmethod
	def _validateSource(self):
		""" Method to validate the source.

			Return:
				(bool): True if the storage is valid
		"""
		raise NotImplementedError("Then 'validate' method needs to be implemented.")

	@abc.abstractmethod
	def _readData(self):
		""" Method to read/get the data from the given storage

			Return:
				(str): The data read from the storage
		"""
		raise NotImplementedError("Then 'read' method needs to be implemented.")

	@abc.abstractmethod
	def writeData(self, data):
		""" Method to write/send the data to the given storage

			Return:
				(bool): True if the storage is valid
		"""
		raise NotImplementedError("Then 'write' method needs to be implemented.")
