"""
"""
import os as _os

from bcSerial import exceptions as _exceptions
from bcSerial.serialization._storages import abstractStorage as _absStorage


class FileStorage(_absStorage.AbstractStorage):
	"""
	"""
	storageName = "file"
	storageType = "ascii"

	def _validateSource(self, source):
		""" method to validate the source

			Args:
				(bool): True if the source is valid
		"""
		if not _os.path.exists(_os.path.dirname(source)):
			raise IOError("Given path directory doesn't exists(%s)" % source)
		return True

	def _validate(self):
		""" Method to validate the storage.

			Return:
				(bool): True if the storage is valid
		"""
		checks = [
			self.source,
			_os.path.exists(_os.path.dirname(self.source)),
			_os.access(_os.path.dirname(self.source), _os.W_OK),
			_os.path.isdir(self.source)
		]
		for check in checks:
			if not check:
				return False

		return True

	def _readData(self):
		""" Method to read/get the data from the given storage

			Return:
				(str): The data read from the storage
		"""
		data = ""
		with open(self.source, "r") as fileHandle:
			data = fileHandle.read()
		return data

	def _writeData(self, data):
		""" Method to write/send the data to the given storage

			Return:
				(bool): True if the storage is valid
		"""
		with open(self.source, "w") as fileHandle:
			fileHandle.write(data)
		return True
