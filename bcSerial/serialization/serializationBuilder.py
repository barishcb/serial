"""
"""
import os as _os
from bcSerial import exceptions as _exceptions
from bcSerial.serialization import serializers as _serializers
from bcSerial.serialization import storages as _storages


class SerializationBuilder(object):
	"""
	"""
	def __init__(self, data=None, serializer=None):
		super(SerializationBuilder, self).__init__()
		self._data = None
		self._serializer = serializer or _serializers.DEFAULT_SERIALIZERCLASS()
		self._serializedData = None

		self.data = data

	@property
	def data(self):
		""" Getter method for the property data

			Returns:
				(list): List of data to be serialized
		"""
		return self._data

	@data.setter
	def data(self, data):
		""" Setter method for the property data

			Args:
				data (list): List of data to be serialized
		"""
		# TODO : Put some validation if need
		self._data = data
		self.encode()

	def setStorage(self, storage):
		""" Menthod to set the storage
		"""
		self.serializer.storage = storage

	def encode(self, data=None):
		""" Menthod to encode the data to serialized using serializer

			Returns:
				(str): serialized data as string
		"""
		data = data or self.data
		try:
			serializedData = self.serializer.encode(data)
		except _exceptions.EncoderError as err:
			raise _exceptions.SerializationError("Decoder Error : %s" % err.message)
		self._serializedData = serializedData
		return serializedData

	def decode(self, serializedData=None):
		""" Menthod to decode the serialized data to python object

			Returns:
				(object): The result of deserialization

			Raises:
				_exceptions.SerializationError: If the data is empty or unable to decode the data
		"""
		serializedData = serializedData or self.serializedData
		if not self.serializedData:
			raise _exceptions.SerializationError("Empty serialization error.")

		try:
			data = self.serializer.decode(serializedData)
		except _exceptions.DecoderError as err:
			raise _exceptions.SerializationError("Decoder Error : %s" % err.message)

		return data

	def read(self, storage=None):
		"""
		"""
		return self._serializer.read(storage)

	def write(self, data=None, storage=None):
		"""
		"""
		data = data or self.data
		self._serializer.write(data, storage)


	serializedData = property(lambda self: self._serializedData)
	serializer = property(lambda self: self._serializer)


def getSerialization(data=None, serializerType=None, storageType=None, source=None):
	""" Create a default serialization object and returns
	"""
	data = data or {}
	serializerType = serializerType or _serializers.DEFAULT_SERIALIZERTYPE
	storageType = storageType or _storages.DEFAULT_STORAGETYPE
	source = source or _os.path.join(_os.getcwd(), "serializedData.slz")

	storage = _storages.getStorageClass(storageType)(source)
	serializer = _serializers.getSerializerClass(serializerType)(storage)
	return SerializationBuilder(data, serializer)
