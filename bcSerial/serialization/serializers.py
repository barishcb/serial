""" Public interface for serializers
"""
from bcSerial import exceptions as _exceptions
from bcSerial.serialization import _serializers
from bcSerial.serialization._serializers import abstractSerializer
from bcSerial.serialization._serializers import cPickleSerializer
from bcSerial.utilities import moduleUtils as _moduleUtils

DEFAULT_SERIALIZERTYPE = "cPickle"


def supportedTypes():
	"""Returns all the supported storages for bcSerial

		Returns:
			(list): list of supported storage classes
	"""
	supportedClasses = _moduleUtils.filterAllClasses(
		_serializers,
		"bcSerial.serialization._serializers",
		[abstractSerializer.AbstractSerializer]
	)
	return [serializerClass.serializerName for serializerClass in supportedClasses ]


def getSerializerClass(serializerType):
	"""[summary]

	[description]

	Arguments:
		serializerType {[type]} -- [description]
	"""
	if serializerType not in supportedTypes():
		_exceptions.UtilitiesError("The given serializer type is not supported %s" % serializerType)
	supportedClasses = _moduleUtils.filterAllClasses(
		_serializers,
		"bcSerial.serialization._serializers",
		[abstractSerializer.AbstractSerializer]
	)
	for serializerClass in supportedClasses:
		if serializerType == serializerClass.serializerName:
			return serializerClass
