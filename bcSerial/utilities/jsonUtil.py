"""
"""
import json as _json


def decode(encodedObj, decoder=None):
	"""
	"""
	if decoder:
		return decoder.decode(encodedObj)
	return _json.loads(encodedObj)
