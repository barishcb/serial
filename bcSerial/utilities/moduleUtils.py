"""
"""
import inspect as _inspect
import importlib as _importlib
import os as _os
import pkgutil as _pkgutil


def filterAllClasses(package, packageNotation, baseClasses):
	"""Find all the classes inside the given package and filter it based on the baseClasses

		Args:
			package (package): Python pacakge contains the classes
			packageNotation (str): import notatiion for the package (eg: bcSerial.serialization)
			baseClasses (list): list of base classes

		Returns:
			(list): list of filtered classes
	"""
	allClasses = []
	pacakgeDirecotry = _os.path.dirname(package.__file__)

	for _, moduleName, _ in _pkgutil.iter_modules([pacakgeDirecotry]):
	    storageModule = _importlib.import_module("%s.%s" % (packageNotation, moduleName))

	    for _, member in _inspect.getmembers(storageModule):
	    	if _inspect.isclass(member):
	    		for baseClass in baseClasses:
	    			if baseClass is not member and issubclass(member, baseClass):
	    				allClasses.append(member)

	return allClasses


