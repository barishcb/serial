""" Utilities to read and wrtie csv files
"""
import os as _os
import csv as _csv

from bcSerial import exceptions as _exceptions


def _readCsvData(csvFile):
	""" Function to read the csv file from storage.

		Args:
			csvFile(str) :  Valid path to csv file.

		Returns:
			(list) : List of dictionaries, having the values.

		Raises:
			exceptions.UtilitiesError: raise when the file doesn't exists
	"""
	result = []
	if not _os.path.exists(csvFile):
		raise _exceptions.UtilitiesError("Unable to find the given csv file(%s)" % csvFile)

	with open(csvFile, "r") as csvFileHandle:
		result = list(_csv.DictReader(csvFileHandle))
	return result


def getPersonalData(csvFile):
	""" Function to read the csv file from storage.

		TODO: Manipulate the date from csv file to create own data type/object.

		Args:
			csvFile(str) :  Valid path to csv file.

		Returns:
			(list) : List of dictionaries, having the values.
	"""
	return _readCsvData(csvFile)
