import prettytable as _prettytable


def printDataAsTable(tableData, keyHeadingMap, printTable=True):
	""" Helper function to print a dictionary of data using PrettyTable.

		Args:
			tableData (list): list of dictionary of data that will be displayed in the shell.
			keyHeadingMap (list): List of tuple, which is in (key, heading) format.

		Keyword Args:
			printTable (bool): Print the table if this is true {Default: True}
				Mostly for testing purpose.
	"""
	tableHeadings = []
	tableColumns = []
	for column, heading in keyHeadingMap:
		tableColumns.append(column)
		tableHeadings.append(heading)

	prettyTable = _prettytable.PrettyTable(tableHeadings)
	prettyTable.align = "l"
	prettyTable.hrules = 1

	for rowData in tableData:
		rowContent = []
		for column in tableColumns:
			rowContent.append(rowData.get(column, ""))
		prettyTable.add_row(rowContent)

	# This if condition is only to get coverage for this line
	print prettyTable if printTable else ""
