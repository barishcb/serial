""" Command line tool for serializing personal informations
"""
import os as _os
import sys as _sys
import argparse as _argparse
import logging as _logging

from bcSerial import common as _common
from bcSerial import exceptions as _exceptions
from bcSerial.utilities import csvUtils as _csvUtils
from bcSerial.utilities import display as _displayUtils
from bcSerial.serialization import serializers as _serializers
from bcSerial.serialization import serializationBuilder as _serializationBuilder

_log = _logging.getLogger(name="bcSerial")
_log.setLevel(_logging.DEBUG)


def serialize(personalDataFile, serializerType=None, serializedFile=None, dryRun=False, testData=False):
	""" Main function to serialize the data that is in the a file

		Args:
			personalDataFile(str): A valid file path, which contains the personal data

		Kwargs:
			serializerType(str): A valid type of serialization supported by the package
			serializedFile(str): A valid file path, which contains the serialized output
			dryRun(boolean): Only serialize the data will not write to file
			testData(boolean): Test the converted data with the original Data
	"""
	personalData = _csvUtils.getPersonalData(personalDataFile)
	serialization = _serializationBuilder.getSerialization(
		data=personalData,
		serializerType=serializerType,
		source=serializedFile
	)
	serialization.encode()

	if testData:
		serialization.decode()
	else:
		serialization.write()

		deserializedData = serialization.read()
		_displayUtils.printDataAsTable(
			deserializedData,
			_common.personalDetailsColumns,
		)

def _validateArgs(parser):
	""" Function to validate all the arguments parsed to the parser

		Args:
			parser (argparse.Namespace) Parser object contains all the arguments

		Return:
			parser (argparse.Namespace) Parser object contains all the arguments

		Raises:
			_exceptions.ArgumentError: When any argumnet parsed is wrong.
	"""
	_log.info("Validating the command options")
	if not parser.dataFile:
		raise _exceptions.ArgumentError("The dataFile should not be empty.")

	if not _os.path.exists(parser.dataFile):
		raise _exceptions.ArgumentError("The dataFile given is not exists(%s)" % parser.dataFile)

	if not parser.serializerType:
		parser.serializerType = _serializers.DEFAULT_SERIALIZERTYPE
	elif parser.serializerType not in _serializers.supportedTypes():
		raise _exceptions.ArgumentError("The given type of serialization is not supported(%s)" % parser.serializerType)

	if parser.serializedFile and not _os.path.exists(_os.path.dirname(parser.serializedFile)):
		raise _exceptions.ArgumentError(
			"The ginven directory to save the serialized data is not exists(%s)" % parser.serializedFile
		)

	_log.info("Validation Completed the command options")
	return parser


def _getParser(args):
	"""	Function take arguments and create a argparse parser object
		out of it.

		Args:
			args (list): List string arguments(Mostly system arguments)

		Returns:
			(argparse.Namespace) Parser object contains all the arguments
	"""
	usage = "Tool to publish the textures exported from mari to maya or katana"
	parser = _argparse.ArgumentParser(description=usage)
	parser.add_argument("-df", "--dataFile", help="A valid file path, which contains the personal data")
	parser.add_argument(
		"-st",
		"--serializerType",
		help="A valid type of serialization supported by the package(Supported Types : %s)" % (
			", ".join(_serializers.supportedTypes())
		)
	)
	parser.add_argument(
		"-sf",
		"--serializedFile",
		nargs='?',
		help="A valid file path, which contains the serialized output"
	)
	parser.add_argument("-d", "--dryRun", action="store_true", help="Only serialize the data will not write to file")
	parser.add_argument("-t", "--testData", action="store_true", help="Test the converted data with the original Data")
	return parser.parse_args(args)


def serializer():
	""" Initialize function for texturePublish tool.
	"""
	parser = _getParser(_sys.argv[1:])
	parser = _validateArgs(parser)
	serialize(
		parser.dataFile,
		serializerType=parser.serializerType,
		serializedFile=parser.serializedFile,
		dryRun=parser.dryRun,
		testData=parser.testData
	)
