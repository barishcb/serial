""" Common variables need for the bcSerial package.
"""
# TODO : Will become dynamic once the serialization factory is ready
supportedTypes = ("cPickle", "json")
defaultType = "cPickle"


personalDetailsColumns = [
	("firstName", "First Name"), ("lastName", "Last Name"),
	("address", "Address"), ("phoneNumber", "Phone Number"),
]
