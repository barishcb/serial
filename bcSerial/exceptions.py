""" Custom Exceptions for bcSerialPackage
"""


class ArgumentError(Exception):
	""" Custom Exception for Command line tools
	"""


class UtilitiesError(Exception):
	""" Custom Exception for utilities
	"""


class SerializationError(Exception):
	""" Custom Exception for Serialization classes
	"""


class EncoderError(Exception):
	""" Custom Exception for Encoder Classes
	"""


class DecoderError(Exception):
	""" Custom Exception for Decoder Classes
	"""


class StorageError(Exception):
	""" Custom Exception for Storage Classes
	"""
