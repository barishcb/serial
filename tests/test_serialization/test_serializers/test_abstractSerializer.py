""" Test cases for bcSerial.cli.serializer module
"""
from datetime import datetime

from tests import testBase

from bcSerial import exceptions as _exceptions
from bcSerial.utilities import csvUtils as _csvUtils
from bcSerial.serialization import storages as _storages
from bcSerial.serialization._serializers import abstractSerializer as _abcSerializer


class MockAbstractSerializer(_abcSerializer.AbstractSerializer):
	"""docstring for MockAbstractSerializer"""

	def encode(self, data):
		""" Method to deserialize the given encoded data. This is a virtal function

			Args:
				data (object): Objects needs to be serialized

			Return:
				(str): Serialized data.

			Raises:
				_exception.EncoderError: When the data is unable to encode
		"""
		return {}

	def decode(self, encodedObject):
		""" Method to deserialize the given encoded data. This is a virtal function

			Args:
				encodedObject (str): Serialized data, which needs to decoded

			Return:
				(object): De serailized data

			Raises:
				_exception.DecoderError: When the data is unable to decode
		"""
		return {}


class Test_SerializerBuilder(testBase.BcSerialTestCase):
	""" Test class for SerializerBuilder module.
	"""

	def setUp(self):
		""" Initialize the data needed for the test cases
		"""
		super(Test_SerializerBuilder, self).setUp()

		self._data = _csvUtils.getPersonalData(self._dataFile)
		self._storage = _storages.getStorageClass("file")(
			self.getTempFiles("someStorage.pkl")
		)
		self._serializer = MockAbstractSerializer(self._storage)

	def test_storage_getter(self):
		""" Testing getter method of storage property of AbstractSerializer
		"""
		self.assertEqual(self._storage, self._serializer.storage)

	def test_storage_setter(self):
		""" Testing setter method of storage property of AbstractSerializer
		"""
		newStorage = _storages.getStorageClass("file")(
			self.getTempFiles("someother.pkl")
		)
		self._serializer.storage = newStorage
		self.assertEqual(newStorage, self._serializer.storage)

	def test_storage_settter_wrongValue(self):
		""" Testing setter method of storage property of AbstractSerializer, with wrong value
			This raises SerializationError.
		"""
		self.assertRaises(_exceptions.SerializationError, setattr, self._serializer, "storage", {})

	def test_read_withNoImplimentation(self):
		""" Testing setter method of storage property of AbstractSerializer, with wrong value
			This raises SerializationError.
		"""
		serializer = _abcSerializer.AbstractSerializer(self._storage)
		self.assertRaises(NotImplementedError, serializer.read)

	def test_write_withNoImplimentation(self):
		""" Testing setter method of storage property of AbstractSerializer, with wrong value
			This raises SerializationError.
		"""
		serializer = _abcSerializer.AbstractSerializer(self._storage)
		self.assertRaises(NotImplementedError, serializer.write, self._data)
