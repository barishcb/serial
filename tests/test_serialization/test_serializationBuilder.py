""" Test cases for bcSerial.cli.serializer module
"""
from datetime import datetime

from tests import testBase

from bcSerial import exceptions as _exceptions
from bcSerial.utilities import csvUtils as _csvUtils
from bcSerial.serialization import serializationBuilder as _serializationBuilder
from bcSerial.serialization import storages as _storages
from bcSerial.serialization import serializers as _serializers


class Test_SerializerBuilder(testBase.BcSerialTestCase):
	""" Test class for SerializerBuilder module.
	"""

	def setUp(self):
		""" Initialize the data needed for the test cases
		"""
		super(Test_SerializerBuilder, self).setUp()

		self._data = _csvUtils.getPersonalData(self._dataFile)
		self._serialization = self.getSerialization(data=self._data)

	def test_data(self):
		""" Testing data property's getter method
		"""
		self.assertEqual(self._data, self._serialization.data)

	def test_data_setter(self):
		""" Testing data property's setter method
		"""
		self._serialization.data = {}
		self.assertEqual({}, self._serialization.data)

	def test_setStorage(self):
		""" Testing setStorage method
		"""
		newFile = self.getTempFiles("someStorage.pkl")
		newStorage = _storages.getStorageClass("file")(newFile)
		self._serialization.setStorage(newStorage)
		self.assertEqual(newStorage, self._serialization.serializer.storage)
		self.assertEqual(newFile, self._serialization.serializer.storage.source)

	def test_encode(self):
		""" Testing setStorage method
		"""
		expected = self.getSerializedData("cPickle")
		actual = self._serialization.encode()

		self.assertEqual(expected, actual)

	def test_encode_withError(self):
		""" Testing setStorage method with error
		"""
		self.assertRaises(
			_exceptions.SerializationError,
			self.getSerialization,
			[datetime.now()],
			serializerType="json"
		)

	def test_decode(self):
		""" Testing setStorage method
		"""
		actual = self._serialization.decode()

		self.assertEqual(self._data, actual)

	def test_decode_withError_EmptyData(self):
		""" Testing setStorage method with error
		"""
		self._serialization._serializedData = None
		self.assertRaises(_exceptions.SerializationError, self._serialization.decode)

