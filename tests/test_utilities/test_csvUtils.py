""" Test cases for bcSerial.cli.serializer module
"""
from tests import testBase

from bcSerial import exceptions
from bcSerial.utilities import csvUtils

validData = [
	{
		'firstName': 'Barish',
		'lastName': 'Chandran',
		'address': '876/1, 12thCross',
		'phoneNumber': '8050921695',
	},
	{
		'firstName': 'Other',
		'lastName': 'Man',
		'address': '878/5, 17thCross',
		'phoneNumber': '8056182240',
	}
]

class Test_Serializer(testBase.BcSerialTestCase):
	""" Test class for serializer module.
	"""

	def setUp(self):
		""" Initialize the data needed for the test cases
		"""
		super(Test_Serializer, self).setUp()
		self._dataFile = self.getResources("personalData.csv")

	def test_readCsvData_validData(self):
		""" Testing the readCsvData with a valid data in it
		"""
		actual = csvUtils._readCsvData(self._dataFile)
		self.assertEqual(validData, actual)

	def test_readCsvData_emptyFile(self):
		""" Testing the readCsvData with a an empty file.
		"""
		actual = csvUtils._readCsvData(self.getResources("emptyFile.csv"))
		self.assertFalse(actual)

	def test_readCsvData_nonExistingFile(self):
		""" Testing the readCsvData with a an empty file.
		"""
		self.assertRaises(exceptions.UtilitiesError, csvUtils._readCsvData, self.getResources("nonExisting.csv"))

	def test_getPersonalData_validData(self):
		""" Testing the getPersonalData with a valid data in it
		"""
		actual = csvUtils.getPersonalData(self._dataFile)
		self.assertEqual(validData, actual)

	def test_getPersonalData_emptyFile(self):
		""" Testing the getPersonalData with a an empty file.
		"""
		actual = csvUtils.getPersonalData(self.getResources("emptyFile.csv"))
		self.assertFalse(actual)

	def test_getPersonalData_nonExistingFile(self):
		""" Testing the getPersonalData with a an empty file.
		"""
		self.assertRaises(exceptions.UtilitiesError, csvUtils.getPersonalData, self.getResources("nonExisting.csv"))

