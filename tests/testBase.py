""" Test base for bcSerial package.
"""
import os as _os
import tempfile as _tempfile

from bcTesting import testBase
from bcSerial.serialization import serializationBuilder as _serializationBuilder
from bcSerial.serialization import storages as _storages
from bcSerial.serialization import serializers as _serializers


class BcSerialTestCase(testBase.TestCase):
	""" Base class for all the tests in the BcSerial package.
	"""
	def setUp(self):
		""" Set up test case
		"""
		super(BcSerialTestCase, self).setUp()
		self._tempDirectory = _tempfile.mkdtemp()

		self._dataFile = self.getResources("personalData.csv")
		self._serializedFile = self.getTempFiles("personalData.pkl")

	def getResources(self, *args):
		""" Method to get the resource paths from the test package.
		"""
		return _os.path.join(
			_os.path.dirname(__file__),
			"resources",
			*args
		)

	def getTempFiles(self, *args):
		""" Method to get the resource paths from the test cases.
		"""
		tempFile = _os.path.join(self._tempDirectory, *args)
		open(tempFile, "w").close()
		return tempFile

	def getSerialization(self, data=None, serializerType=None, storageType=None):
		""" Create a default serialization object and returns
		"""
		data = data or {}
		serializerType = serializerType or "cPickle"
		storageType = storageType or "file"

		storage = _storages.getStorageClass(storageType)(self._serializedFile)
		serializer = _serializers.getSerializerClass(serializerType)(storage)
		return _serializationBuilder.SerializationBuilder(data, serializer)

	def getSerializedData(self, serializerType):
		""" Get existing serialized data from resource for testing
		"""
		ext = "pkl" if serializerType == "cPickle" else "json"
		serializedFile = self.getResources("personalData.%s" % ext)
		with open(serializedFile, "r") as fileHandle:
			return fileHandle.read()
