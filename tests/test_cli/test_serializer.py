""" Test cases for bcSerial.cli.serializer module
"""
from tests import testBase

from bcSerial import exceptions
from bcSerial.cli import serializer


class Test_Serializer(testBase.BcSerialTestCase):
	""" Test class for serializer module.
	"""

	def setUp(self):
		""" Initialize the data needed for the test cases
		"""
		super(Test_Serializer, self).setUp()
		self._dataFile = self.getResources("personalData.csv")
		self._serializedFile = self.getTempFiles("personalData.slz")
		self._serializerType = "cPickle"
		self.injectArguments(self._dataFile, self._serializerType, self._serializedFile)
		self.monkeyPatchObject(serializer._sys, "exit", lambda func: None)

	def injectArguments(self, dataFile, serializerType=None, serializedFile=None, dryRun=False, testRun=False):
		""" Build and inject argument from the given parameters.
		"""
		arguments = ["serialize", "--dataFile", dataFile]
		if serializerType:
			arguments.extend(["--serializerType", serializerType])
		if serializedFile:
			arguments.extend(["--serializedFile", serializedFile])
		if dryRun:
			arguments.append("--dryRun")
		if testRun:
			arguments.append("--testRun")

		self.monkeyPatchObject(serializer._sys, "argv", arguments)

	def test_serializer_valid(self):
		""" Test case with valid arguments
		"""
		self.assertFalse(serializer.serializer())

	def test_serializer_nonExistingDataFile(self):
		""" Test case with non existing data file path
		"""
		self.injectArguments("/non/existing/File", self._serializerType, self._serializedFile)
		self.assertRaises(exceptions.ArgumentError, serializer.serializer)

	def test_serializer_emptyDataFilePath(self):
		""" Test case with datafile path as empty
		"""
		self.injectArguments("", self._serializerType, self._serializedFile)
		self.assertRaises(exceptions.ArgumentError, serializer.serializer)

	def test_serializer_wrongSerializerType(self):
		""" Test case with wrong serialize type
		"""
		self.injectArguments(self._dataFile, "unKnownSerialization", self._serializedFile)
		self.assertRaises(exceptions.ArgumentError, serializer.serializer)

	def test_serializer_NoneSerializerType(self):
		""" Test case with empty serializerType
		"""
		self.injectArguments(self._dataFile, serializedFile=self._serializedFile)
		self.assertFalse(serializer.serializer())

	def test_serializer_NoneSerializedFile(self):
		""" Test case with empty serializedFile
		"""
		self.injectArguments(self._dataFile)
		self.assertFalse(serializer.serializer())

	def test_serializer_WrongSerializedFile(self):
		""" Test case with empty serializedFile
		"""
		self.injectArguments(self._dataFile, serializedFile="/non/existing/File")
		self.assertRaises(exceptions.ArgumentError, serializer.serializer)
